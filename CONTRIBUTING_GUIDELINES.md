# Contributing Guidelines

As an open source plattform and project, we welcome contributions from our community in any form. You can do so by submitting bug reports, feature requests, or by directly contributing to Coscine's source code.

## Submitting bug reports or feature requests

Reporting a bug :bug: or submitting a feature request in Coscine's GitLab repository is straightforward: 

1. **Check for existing issue**: Check if the bug or feature has already been reported/requested by searching the [Coscine issue tracker]().
2. **Open a new issue**: If not, open a new issue by clicking on the "New issue" button and select "Bug" or "Feature request" as the issue type from the existing templates.
3. **Write a descriptive title and description**: Write a descriptive title that summarizes the issue/request and provide as much detail as possible in the description. Include steps to reproduce the bug, or a clear explanation of the feature request.
4. **Add relevant labels**: Add relevant labels to the issue, such as "bug", "enhancement", or "documentation".
5. **Include screenshots or code snippets**: If possible, include screenshots or code snippets to better illustrate the issue/request.
6. **Submit the issue**: Submit the issue and wait for a response from the repository maintainers.
7. **Be responsive to follow-up**: Be responsive to any follow-up questions or requests for more information from the maintainers, and help to debug or clarify the issue/request.

```mermaid
graph LR;
    classDef APP fill:#ff6e40,color:#fff

    A[Check for existing issue]:::APP --> B[Open a new issue]:::APP;
    B --> C[Write a descriptive title and description]:::APP;
    C --> D[Add relevant labels]:::APP;
    D --> E[Include screenshots or code snippets]:::APP;
    E --> F[Submit the issue]:::APP;
    F --> G[Be responsive to follow-up]:::APP;
```

## Contributing to Coscine's source code

If you have good knowledge of our used technologies and like to have some hands-on experience, the provided guide should aid you in making a valuable contribution to the source code of Coscine:

1. **Read the project's documentation**: Before contributing, make sure to read the project's documentation to understand the guidelines, contribution process, and coding standards. This will help you avoid common mistakes and ensure that your code meets our requirements.
2. **Fork the project and implement your changes**: Make the necessary changes in your local repository using your preferred code editor.
3. **Test your changes**: Test your changes locally to make sure they work as expected.
4. **Commit and push your changes**: Commit your changes with a descriptive commit message that explains the changes you made.
5. **Create a merge request**: Create a merge request on the project's GitLab page to submit your changes to the project. Make sure to describe the changes you made, explain why you made them, and include any relevant documentation or test cases. Remember to follow the following commit convention, so that your changes can be reflected in the project's release log:
    - Apply a merge request title according to the commit convention:
      * Patch _(1.0.0 --> 1.0.**1**)_ keywords - `Fix:`
          ```
          Fix: Project name not displayed properly
          Docs: Method explaination
          ```
      * Minor _(1.0.0 --> 1.**1**.0)_ keywords - `New:`, `Update:`
          ```
          New: User table
          Update: Quota calculation
          ```
      * Major _(1.0.0 --> **2**.0.0)_ keywords - `Breaking:`
          ```
          Breaking: Migrated to latest .NET version
          ```
      - Example: `Fix: Project name not displayed properly`
6. **Respond to feedback**: The maintainers of the project may provide feedback on your merge request. Address their comments and make any necessary changes to your code.
7. **Get your changes merged**: Once your changes have been approved, the maintainers will merge them into the project's `dev` branch for hands-on testing from our team. Congratulations, you've contributed to our open source project!

```mermaid
graph LR;
    classDef APP fill:#00549f,color:#fff

    A(Read the project's documentation):::APP --> B(Fork the project and implement your changes):::APP;
    B --> C(Test your changes):::APP;
    C --> D(Commit and push your changes):::APP;
    D --> E(Create a merge request):::APP;
    E --> F(Respond to feedback):::APP;
    F --> G(Get your changes merged):::APP;
```

Remember that contributing to open source projects is a collaborative effort, so it's important to communicate clearly, be respectful of others' opinions, and follow the project's guidelines. 

Happy contributing! ✨
