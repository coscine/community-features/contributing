# Contributing to Coscine

Before contributing to Coscine please review our Contributor Agreement and 
the Developer's Certificate of Origin below.

To indicate that you have acknowledged and agreed to the terms please [create an issue in this project](https://git.rwth-aachen.de/coscine/collaboration/issues/-/issues/new?issuable_template=acknowledge_contributing&issue[title]=I+Acknowledge+the+Contributing+Agreement).

## Contributing Guidelines

Coscine follows contributing guidelines. See [Contributing Guidelines](https://git.rwth-aachen.de/coscine/community-features/contributing/-/blob/master/CONTRIBUTING_GUIDELINES.md?ref_type=heads) for details.

## Contributor Argeement

Coscine follows a contributor agreement. See [Contributor Agreement](CONTRIBUTOR_AGREEMENT.md) for details

## Developer's Certificate of Origin 1.1

By making a contribution to this project, I certify that:

* (a) The contribution was created in whole or in part by me and I
  have the right to submit it under the open source license
  indicated in the file; or

* (b) The contribution is based upon previous work that, to the best
  of my knowledge, is covered under an appropriate open source
  license and I have the right under that license to submit that
  work with modifications, whether created in whole or in part
  by me, under the same open source license (unless I am
  permitted to submit under a different license), as indicated
  in the file; or

* (c) The contribution was provided directly to me by some other
  person who certified (a), (b) or (c) and I have not modified
  it.

* (d) I understand and agree that this project and the contribution
  are public and that a record of the contribution (including all
  personal information I submit with it, including my sign-off) is
  maintained indefinitely and may be redistributed consistent with
  this project or the open source license(s) involved.
