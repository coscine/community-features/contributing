# Local NuGet Workflow Guide

To include local NuGet packages, you need to download and install (and set the path variable) for NuGet.

Get it from here: [nuget.org/downloads](https://www.nuget.org/downloads)

## Usage
For more detailed instructions see: [Creating and using a local nuget package repository](https://medium.com/@churi.vibhav/creating-and-using-a-local-nuget-package-repository-9f19475d6af8)

The basic steps are:
1.   Create the NuGet package using Visual Studio or cake build script:
        1. For Visual Studio:
            * Create a .NET standard class library.
            * In project properties > package => fill assembly details.
            * In the solution explorer, right click the project => click "Pack".
        2. For cake:
            * Add our .nuspec file template, if not present
            * Run the command .\build.ps1 -Configuration Release -Target Build to build the project under release
            * Run the command .\build.ps1 -Configuration Release -Target NugetPack to pack it
2.  Add the package to the local NuGet repository:
    ```
    >nuget add <package_id>.nupkg -source <source_path>\<nuget_repo_name>
    ```
    You can navigate to the source path (<source_path>\\<nuget_repo_name>) and call
    ```
    >tree
    ```
    and you should see the added package in the repository tree.
3.  To import the local package, open the project that you want to include the package in Visual Studio.
4.  In the solution explorer, right click the solution => click "manage NuGet packages for solution..."
5.  Click settings (little gear icon near "Manage Packages for Solution" in the top right corner).
6.  Add the local package (click "+", set the source path to the local package).
7.  Move the local package you just added above the nuget.org path to give it higher priority.
8.  Now you should be able to find, install and use the local package like any other nuget package.