# Contribution guide

See [ESLint Convention](ESLintConvention) for our commit message convention.

See [Local NPM workflow](npm) for our recommendation on how to work with local NPM packages.

See [Local NuGet workflow](nuget) for our recommendation on how to work with local NuGet packages.