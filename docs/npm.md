# Local NPM Workflow Guide

For using local NPM libraries in your projects, you can use "npm link" to include them.

## Usage

1.  First you need to navigate to the needed NPM project
2.  After that you execute "npm link" for creating a global symlink
3.  Then you navigate to the project which needs the local project
4.  With "npm link <project name>" you can then finally include the needed project locally

## Example

```
cd <locally-needed-project-root>
npm link # create a global symlink to the local needed project
cd <project-needing-local-project-root>
npm link <locally-needed-project-name> # create a symlink locally to global needed project symlink
```