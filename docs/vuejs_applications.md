# While working on VueJs applications

When working with a VueJs application take a look at the following list and make the necessary changes to the application

1. Update the dependencies (use ncu module to check for the newest version)
2. Remove unused or unnecessary dependencies (use depcheck for an indication but check yourself since the indication are often wrong)
3. Check if the devdependencies are useful and used correctly
4. Check if there are utility functions that could be moved to the app-util
5. Check if there is code that can be replaced with functions fro the app-util
6. Remove the it-center bootstrap css files, these are included globally
7. Move the functions to the earliest possible event in the life cycle
8. Check if request to the api can be performed in parallel
9. Check for unused properties and data